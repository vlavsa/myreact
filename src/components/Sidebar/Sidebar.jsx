
import { NavLink } from "react-router-dom";
import classes from "./Sidebar.module.css";

const Sidebar = () => {
  return (
    <nav className={classes.sidebar}>
      <div className={classes.logo_content}>
        <div className={classes.logo}>
          <i className="bx bxs-invader"></i>
          <div className={classes.logo_name}>VLAVSA</div>
        </div>
        <i className="bx bx-menu" onClick={ openSideBar } id={classes["btn"]}></i>
      </div>

      <ul className={classes.nav_list}>
        {/* Search */}
        <li>
          <NavLink to="#">
            <i  onClick={ openSideBar } className={`bx bx-search-alt ${classes.box_search}`}></i>
            <input type="text" placeholder="Search..." />
          </NavLink>
          <span className={classes.tooltip}>Search</span>
        </li>
        {/* Chess */}
        <li>
          <NavLink to="chess">
            <i className="bx bxs-chess"></i>
            <span className={classes.links_name}>Chess</span>
          </NavLink>
          <span className={classes.tooltip}>Chess</span>
        </li>
        {/* Dashbord */}
       
        <li>        
          <NavLink to="dashboard">
            <i className="bx bx-grid-alt"></i>
            <span className={classes.links_name}>Dashboard</span>
          </NavLink>
          <span className={classes.tooltip}>Dashboard</span>
        </li>
        {/* Product */}
        <li>
          <NavLink to="product">
            <i className="bx bx-package"></i>
            <span className={classes.links_name}>Products</span>
          </NavLink>
          <span className={classes.tooltip}>Products</span>
        </li>
        {/* Order */}
        <li>
          <NavLink to="order">
            <i className="bx bx-cart"></i>
            <span className={classes.links_name}>Order</span>
          </NavLink>
          <span className={classes.tooltip}>Order</span>
        </li>
        {/* Analitics */}
        <li>
          <NavLink to="analitics">
            <i className="bx bx-pie-chart"></i>
            <span className={classes.links_name}>Analitics</span>
          </NavLink>
          <span className={classes.tooltip}>Analitics</span>
        </li>
        {/* User */}
        <li>
          <NavLink to="user">
            <i className="bx bx-user-circle"></i>
            <span className={classes.links_name}>User</span>
          </NavLink>
          <span className={classes.tooltip}>User</span>
        </li>
        {/* Messages */}
        <li>
          <NavLink to="messages">
            <i className="bx bx-chat"></i>
            <span className={classes.links_name}>Messages</span>
          </NavLink>
          <span className={classes.tooltip}>Messages</span>
        </li>
        <div className={classes.profile_content}>
          <div className={classes.profile}>
            <div className={classes.profile_details}>
              <div className={classes.profile_photo}>
                <img src="https://sun9-east.userapi.com/sun9-35/s/v1/if2/aTg3MY52YZwny8FrBuyA77gKyVlqI57r4i-mVwmoK7ufXsnFA5HCzF_yRuC_DnErrK7NV2o_xoANBvsXDM_2XLpb.jpg?size=960x720&quality=96&type=album" alt="" />
              </div>
              <div className={classes.name_job}>
                <div className={classes.name}>Vladislav Savichev</div>
                <div className={classes.job}>Frontend developer</div>
              </div>
            </div>
            <i className="bx bx-log-out" id={classes["log_out"]}></i>
          </div>
        </div>
      </ul>
    </nav>
  );
};


const openSideBar = () => {
  document.querySelector(".app-wrapper").classList.toggle("active")
  document.querySelector(`.${classes.sidebar}`).classList.toggle(`${classes.activeSidebar}`)
} 


export default Sidebar;
