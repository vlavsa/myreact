import  classes from "./Header.module.css";


const Header = () => {
    return <header className={classes.wrapper}>
    <img
      src="https://sun9-east.userapi.com/sun9-35/s/v1/if2/aTg3MY52YZwny8FrBuyA77gKyVlqI57r4i-mVwmoK7ufXsnFA5HCzF_yRuC_DnErrK7NV2o_xoANBvsXDM_2XLpb.jpg?size=960x720&quality=96&type=album"
      alt=""
    />
  </header>
};
export default Header;