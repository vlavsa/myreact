import goods from "./Products.module.css";

const CardProduct = (props) => {
  console.log("CardProducts", props);
  return (
    <div className={goods.products_card}>
      <img src={props.post.images.img} alt={props.post.images.alt} />
      <div className={goods.name_price}>
        <div className="name_product">{props.post.title}</div>
        <div className="price_product">{props.post.price} руб.</div>
      </div>
    </div>
  );
};

export default CardProduct;
