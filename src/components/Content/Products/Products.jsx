import CardProduct from "./Product";
import goods from "./Products.module.css";

const Block_of__goods = (props) => {
    console.log("Block_of__goods", props)
    return (
        <div className={goods.block_of__goods}>
            <div className={goods.logo_name}>
                <div className={goods.category_name}>Наименование категории</div>
            </div>
            <div className={goods.list_of__goods}>
                < CardProduct post={{id:1, title: 'Гусь моржовый', price:1500, images:{img:'https://sun9-east.userapi.com/sun9-35/s/v1/if2/aTg3MY52YZwny8FrBuyA77gKyVlqI57r4i-mVwmoK7ufXsnFA5HCzF_yRuC_DnErrK7NV2o_xoANBvsXDM_2XLpb.jpg?size=960x720&quality=96&type=album', alt:'photo   Test'}}}/>
            </div>
        </div>
    )
}

export default Block_of__goods;