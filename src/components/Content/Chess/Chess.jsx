import { useState } from "react";

const Chess = () => {
  let [likes, setLikes] = useState(0);
  let [value, setValue] = useState("ВВЕДИТЕ ТЕКСТ");

  function increment() {
    setLikes((likes += 1));
  }
  function decrement() {
    setLikes((likes -= 1));
  }
  return (
    <div>
      <h2>{likes}</h2>
      <h3>{value}</h3>
      <input
        type="text"
        value={value}
        onChange={(event) => setValue(event.target.value)}
      />
      <br />
      <button onClick={increment}>increment</button>
      <button onClick={decrement}>decrement</button>
    </div>
  );
};
export default Chess;
 