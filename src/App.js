import { Routes, Route, BrowserRouter} from "react-router-dom";
import "./App.css";
import Chess from "./components/Content/Chess/Chess";
import Dashboard from "./components/Content/Dashboard/Dashboard";
import Product from "./components/Content/Products/Products";
import Order from "./components/Content/Order/Order";
import Analitics from "./components/Content/Analitics/Analitics";
import User from "./components/Content/User/User";
import Messages from "./components/Content/Messages/Messages";

import Header from "./components/Header/Header";
import Sidebar from "./components/Sidebar/Sidebar";



function App(props) {
  return (
    <BrowserRouter>
      <div className="app-wrapper">
        <Header />
        <Sidebar />
        <div className="app-contents">
          <div className="content">
            <Routes>
                <Route path="/chess" element={<Chess />}/>
                <Route path="/dashboard" element={<Dashboard />}/>
                <Route path="/product" element={<Product />}/>
                <Route path="/order" element={<Order />}/>
                <Route path="/analitics" element={<Analitics />}/>
                <Route path="/user" element={<User />}/>
                <Route path="/messages" element={<Messages />}/>
              </Routes>
          </div> 
          
        </div>
      </div>
    </BrowserRouter>
);
}



export default App;
 